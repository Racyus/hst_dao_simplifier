package br.com.hst.exceptions;

/*
 * @author Guilherme Ferreira
 */
public class HDSException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HDSException(String message) {
        super(message);
    }

    public HDSException(String message, Throwable cause) {
        super(message, cause);
    }

}
