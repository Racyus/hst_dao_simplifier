package br.com.hst.exceptions;

/*
 * @author Guilherme Ferreira
 */
public class HDSAnnotationException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public HDSAnnotationException(String message) {
        super(message);
    }

    public HDSAnnotationException(String message, Throwable cause) {
        super(message, cause);
    }

}
