package br.com.hst;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;

class HDSField {

	protected static final Short PK = 0;
	protected static final Short FK = 1;
	protected static final Short AR = 2;
	protected static final Short NF = 3;

	private String attBdName = null;
	
	private String alias = null;

	private String sequence = null;

	private Method getMethod = null;
	
	private Method setMethod = null;

	private Field field = null;

	private Short type = null;

	protected HDSField(String sequence,Method getMethod, Method setMethod, Field field, Short type) {
		super();
		if (type == 3) {
			this.attBdName = HDSRegex.dbName(field.getName());
		} else {
			this.attBdName = HDSRegex.dbName(field.getName().substring(0, field.getName().length() - 2));
		}
		this.sequence = sequence;
		this.getMethod = getMethod;
		this.setMethod = setMethod;
		this.field = field;
		this.type = type;
	}

	protected Boolean haveSequence() {
		if (sequence != null)
			return true;
		else
			return false;
	}
	
	protected Method getGetMethod() {
		return getMethod;
	}

	protected String getAlias() {
		return alias;
	}

	protected void setAlias(String alias) {
		this.alias = alias;
	}

	protected String getAttBdName() {
		return attBdName;
	}

	protected Field getField() {
		return field;
	}

	protected String getSequence() {
		return sequence;
	}

	protected Method getSetMethod() {
		return setMethod;
	}

	protected Short getType() {
		return type;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HDSField row = (HDSField) o;
		return Objects.equals(this.sequence, row.sequence) && Objects.equals(this.setMethod, row.setMethod)
				&& Objects.equals(this.type, row.type);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sequence, setMethod, type);
	}

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(Idt.T1 + "Sequence: ").append(toIndentedString(sequence)).append(System.lineSeparator());
		str.append(Idt.T1 + "Method: ").append(toIndentedString(setMethod.getName())).append(System.lineSeparator());
		str.append(Idt.T1 + "Type: ").append(toIndentedString(type)).append(System.lineSeparator());
		str.append(Idt.T1 + "Field: ").append(toIndentedString(field.getName())).append(System.lineSeparator());
		return str.toString();
	}

	protected String toString(String tab) {
		StringBuffer str = new StringBuffer();
		if (sequence != null)
			str.append(tab + "Sequence: ").append(toIndentedString(sequence)).append(System.lineSeparator());
		if (setMethod != null)
			str.append(tab + "Method: ").append(toIndentedString(setMethod.getName())).append(System.lineSeparator());
		if (type != null)
			str.append(tab + "Type: ").append(toIndentedString(type)).append(System.lineSeparator());
		if (field != null)
			str.append(tab + "Field: ").append(toIndentedString(field.getName())).append(System.lineSeparator());
		if (attBdName != null)
			str.append(tab + "AttBdName: ").append(toIndentedString(attBdName)).append(System.lineSeparator());
		return str.toString();
	}

	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace(System.lineSeparator(), System.lineSeparator() + "    ");
	}

}
