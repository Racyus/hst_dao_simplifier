package br.com.hst;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Objects;

class HDSSon {

	private Method setMethod = null;

	private Method getMethod = null;

	private Field field = null;

	private Type type = null;

	private Type superType = null;

	public HDSSon(Method setMethod, Field field) {
		super();
		this.setMethod = setMethod;
		this.field = field;
	}

	public HDSSon(Method setMethod, Method getMethod, Field field, Type type, Type superType) {
		super();
		this.setMethod = setMethod;
		this.getMethod = getMethod;
		this.field = field;
		this.type = type;
		this.superType = superType;
	}

	public Method getSetMethod() {
		return setMethod;
	}

	public Method getGetMethod() {
		return getMethod;
	}

	public Field getField() {
		return field;
	}

	public Type getType() {
		return type;
	}

	public Type getSuperType() {
		return superType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HDSSon row = (HDSSon) o;
		return Objects.equals(this.setMethod, row.setMethod) && Objects.equals(this.getMethod, row.getMethod)
				&& Objects.equals(this.field, row.field) && Objects.equals(this.type, row.type)
				&& Objects.equals(this.superType, row.superType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(setMethod, getMethod, field, type, superType);
	}

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(Idt.T1 + "SetMethod: ").append(toIndentedString(setMethod.getName())).append(System.lineSeparator());
		str.append(Idt.T1 + "GetMethod: ").append(toIndentedString(getMethod.getName())).append(System.lineSeparator());
		str.append(Idt.T1 + "Field: ").append(toIndentedString(field.getName())).append(System.lineSeparator());
		str.append(Idt.T1 + "Type: ").append(toIndentedString(type))
				.append(System.lineSeparator());
		str.append(Idt.T1 + "SuperType: ").append(toIndentedString(superType))
				.append(System.lineSeparator());
		return str.toString();
	}

	public String toString(String tab) {
		StringBuffer str = new StringBuffer();
		if (setMethod != null)
			str.append(Idt.T1 + "SetMethod: ").append(toIndentedString(setMethod.getName()))
					.append(System.lineSeparator());
		if (getMethod != null)
			str.append(Idt.T1 + "GetMethod: ").append(toIndentedString(getMethod.getName()))
					.append(System.lineSeparator());
		if (field != null)
			str.append(Idt.T1 + "Field: ").append(toIndentedString(field.getName())).append(System.lineSeparator());
		if (type != null)
			str.append(Idt.T1 + "Type: ").append(toIndentedString(type))
					.append(System.lineSeparator());
		if (superType != null)
			str.append(Idt.T1 + "SuperType: ").append(toIndentedString(superType))
					.append(System.lineSeparator());
		return str.toString();
	}

	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace(System.lineSeparator(), System.lineSeparator() + "    ");
	}

}
