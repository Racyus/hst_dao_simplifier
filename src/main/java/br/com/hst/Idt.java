package br.com.hst;

abstract class Idt {
	public static final String BR = System.lineSeparator();
	public static final String BR2 = BR+BR;
	public static final String T1 ="\t";
	public static final String T2 ="\t\t";
	public static final String T3 ="\t\t\t";
	public static final String T4 ="\t\t\t\t";
	public static final String BRT1 = BR + T1; 
	public static final String BRT2 = BR + T2; 
	public static final String BRT3 = BR + T3; 
	public static final String BRT4 = BR + T4; 
	public static final String COMMA = " , ";
	public static final String BRT1C = COMMA + BRT1;
	public static final String BRT2C = COMMA + BRT2;
	public static final String BRT3C = COMMA + BRT3;
	public static final String BRT4C = COMMA + BRT4;
	public static final String DOT = ".";

}
