package br.com.hst;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import br.com.hst.annotations.HDSIgnore;
import br.com.hst.annotations.HDSSequence;

/**
 * <p>
 * Esta classe é responsável por refletir um objeto TO.
 * <p>
 * São Mapeados:
 * 
 * <ul>
 * <li>O typo classe
 * <li>O nome da classe para extrair o nome da tabela
 * <li>Um Construtor sem parametros.
 * <ul>
 * 
 * @see HDSField {
 *      <li>Os nomes dos atributos dos objetos
 *      <li>Os nomes dos atributos no banco de dados
 *      <li>Os metodos seters
 *      <li>Os fields
 *      <li>O tipo de cada objeto(ex:primaryKey, foreign key...)
 *      </ul>
 *      }
 *      <li>Os objetos que são filhos desse objeto
 *      </ul>
 *
 * @param <T> the modelTO object
 *
 * @author Guilherme Calvi Ferreira
 * @since 0.1
 */

class HDSModelReflector<T>
{

	private Class<T> clazz = null;

	private Constructor<T> constructor = null;
	private String table = null;

	private ArrayList<HDSField> fieldsList = new ArrayList<>();
	private Map<String, Method> setMethodsMap = new HashMap<>();
	private Map<String, Method> getMethodsMap = new HashMap<>();
	private ArrayList<HDSSon> sonsList = new ArrayList<>();

	protected HDSModelReflector(Class<T> clazz)
	{
		this.clazz = clazz;
		tableAnnotation();
		getSetMethods();
		getAllFields();
	}

	@SuppressWarnings("unchecked")
	private void tableAnnotation()
	{
		if (!clazz.isAnnotationPresent(HDSIgnore.class))
		{
			table = HDSRegex.dbName(clazz.getSimpleName());
			Constructor<?>[] constructors = clazz.getConstructors();
			for (int i = 0; i < constructors.length; i++)
			{
				if (constructors[i].getParameterCount() == 0)
				{
					constructor = (Constructor<T>) constructors[i];
				}
			}
		}
		else
		{
			System.out.println("Não há anotação de tabela.");
		}
	}

	private void getSetMethods()
	{
		Method[] methods = clazz.getMethods();
		for (int i = 0; i < methods.length; i++)
		{
			if (methods[i].getName()
					.startsWith("set"))
			{
				setMethodsMap.put(methods[i].getName()
						.substring(4), methods[i]);
			}
			else if (methods[i].getName()
					.startsWith("get"))
			{
				getMethodsMap.put(methods[i].getName()
						.substring(4), methods[i]);
			}
		}
	}

	private void getAllFields()
	{
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++)
		{

			if (!fields[i].isAnnotationPresent(HDSIgnore.class) && !Modifier.isStatic(fields[i].getModifiers()))
			{
				if (Collection.class.isAssignableFrom(fields[i].getType())
						|| Map.class.isAssignableFrom(fields[i].getType()) || fields[i].getType()
								.getPackageName()
								.endsWith("model"))
				{
					Type type = fields[i].getGenericType();
					String fieldName = fields[i].getName()
							.substring(1);
					if (type instanceof ParameterizedType)
					{
//						ParameterizedType pt = (ParameterizedType) type;
//						System.out.println("raw type: " + pt.getRawType());
//						System.out.println("owner type: " + pt.getOwnerType());
//						System.out.println("actual type args:");
						for (Type t : ((ParameterizedType) type).getActualTypeArguments())
						{
//							System.out.println("    " + t);
							Class<?> claze = (Class<?>) t;
							if (claze.getPackageName()
									.endsWith("model"))
							{
								sonsList.add(new HDSSon(setMethodsMap.get(fieldName), getMethodsMap.get(fieldName),
										fields[i], t, fields[i].getType()));
							}
						}
					}
					else
					{
						sonsList.add(new HDSSon(setMethodsMap.get(fieldName), fields[i]));
					}
				}
				else
				{
					Short type = null;
					if (fields[i].getName()
							.endsWith(HDSRegex.PK))
					{
						type = HDSField.PK;
					}
					else if (fields[i].getName()
							.endsWith(HDSRegex.FK))
					{
						type = HDSField.FK;
					}
					else if (fields[i].getName()
							.endsWith(HDSRegex.AR))
					{
						type = HDSField.AR;
					}
					else
					{
						type = HDSField.NF;
					}
					fieldsList.add(new HDSField(getSequence(fields[i]), getMethodsMap.get(fields[i].getName()
							.substring(1)), setMethodsMap.get(
									fields[i].getName()
											.substring(1)),
							fields[i], type));
				}
			}
		}
	}

	private String getSequence(Field field)
	{
		if (field.isAnnotationPresent(HDSSequence.class))
		{
			return field.getAnnotation(HDSSequence.class)
					.sequence();
		}
		else
			return null;
	}

	protected Constructor<T> getConstructor()
	{
		return constructor;
	}

	protected ArrayList<HDSField> getFieldsList()
	{
		return fieldsList;
	}

	protected ArrayList<HDSSon> getSonsList()
	{
		return sonsList;
	}

	protected String getTable()
	{
		return table;
	}

}
