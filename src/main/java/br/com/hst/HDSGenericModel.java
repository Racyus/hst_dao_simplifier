package br.com.hst;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * <p>
 * Esta classe é responsável por mapear um objeto TO.
 * <p>
 * São Mapeados:
 * 
 * <ul>
 * <li>O typo classe
 * <li>O nome da classe para extrair o nome da tabela
 * <li>Um Construtor sem parametros.
 * <ul>
 * 
 * @see HDSField {
 *      <li>Os nomes dos atributos dos objetos
 *      <li>Os nomes dos atributos no banco de dados
 *      <li>Os metodos seters
 *      <li>Os fields
 *      <li>O tipo de cada objeto(ex:primaryKey, foreign key...)
 *      </ul>
 *      }
 *      <li>Os objetos que são filhos desse objeto
 *      </ul>
 *
 * @param <T> the modelTO object
 *
 * @author Guilherme Calvi Ferreira
 * @since 0.1
 */

class HDSGenericModel<T> {

	private Class<T> clazz = null;
	private Constructor<T> constructor = null;

	private String table = null;
	private ArrayList<HDSField> fieldsList = new ArrayList<>();
	private ArrayList<HDSSon> sonsList = new ArrayList<>();

	protected HDSGenericModel(Class<T> clazz) {
		this.clazz = clazz;
		HDSModelReflector<T> hmr = new HDSModelReflector<>(clazz);
		table = hmr.getTable();
		constructor = hmr.getConstructor();
		fieldsList = hmr.getFieldsList();
		sonsList = hmr.getSonsList();
	}

	protected StringBuilder getFields() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < fieldsList.size(); i++) {
			str.append(fieldsList.get(i).getAttBdName());
			if(i!=fieldsList.size()-1)
				str.append(Idt.COMMA);
		}
		return str;
	}

	protected T getResultSet(ResultSet rs) {
		T newInstance = null;
		try {
			newInstance = constructor.newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < fieldsList.size(); i++) {
			HDSField v = fieldsList.get(i);
			try {
				methodInvoker(v.getSetMethod(), newInstance, rs, v.getAttBdName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return newInstance;
	}

	protected String getTable() {
		return table;
	}

	private void methodInvoker(Method method, T objInvokeOn, ResultSet rs, String attribute) {
		Class<?> type = method.getParameters()[0].getType();
		try {
			if (type == String.class) {
				method.invoke(objInvokeOn, rs.getString(attribute));
			} else if (type == Long.class) {
				method.invoke(objInvokeOn, rs.getLong(attribute));
			} else if (type == Double.class) {
				method.invoke(objInvokeOn, rs.getDouble(attribute));
			} else if (type == Integer.class) {
				method.invoke(objInvokeOn, rs.getInt(attribute));
			} else if (type == Boolean.class) {
				method.invoke(objInvokeOn, rs.getBoolean(attribute));
			} else if (type == Date.class) {
				method.invoke(objInvokeOn, rs.getDate(attribute));
			} else if (type == BigDecimal.class) {
				method.invoke(objInvokeOn, rs.getBigDecimal(attribute));
			} else if (type == Byte.class) {
				method.invoke(objInvokeOn, rs.getByte(attribute));
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException e) {
			e.printStackTrace();
		}
	}

	protected HDSSon amIYourSon(Object obj) {
		if (!sonsList.isEmpty()) {
			for (int i = 0; i < sonsList.size(); i++) {
				if (sonsList.get(i).getType().equals(obj.getClass())) {
					return sonsList.get(i);
				}
			}
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map<Type, ArrayList<Type>> getAllYourDescendants(HDSGenericModel... speakers) {
		Map<Type, ArrayList<Type>> decendants = new HashMap<Type, ArrayList<Type>>();
		for (int i = 0; i < speakers.length; i++) {
			decendants.put(speakers[i].getClazz(), speakers[i].getSons());
		}
		return decendants;
	}

	private ArrayList<HDSSon> getSons() {
		return sonsList;
	}

	protected ArrayList<HDSField> getFieldsArrayList() {
		return fieldsList;
	}

	protected ArrayList<String> getKeysList() {
		ArrayList<String> list = new ArrayList<>();
		fieldsList.forEach(v -> {
			if (v.getType() == 0 || v.getType() == 1) {
				list.add(v.getAttBdName());
			}
		});
		return list;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		@SuppressWarnings("unchecked")
		HDSGenericModel<T> speaker = (HDSGenericModel<T>) o;
		if (!speaker.clazz.equals(clazz)) {
			return false;
		}
		return Objects.equals(this.table, speaker.table) && Objects.equals(this.fieldsList, speaker.fieldsList);
	}

	@Override
	public int hashCode() {
		return Objects.hash(clazz, table, fieldsList);
	}

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("class " + clazz.getSimpleName() + " - table: " + table.toLowerCase() + " {");
		for (int i = 0; i < fieldsList.size(); i++) {
			HDSField v = fieldsList.get(i);
			str.append(Idt.BRT1 + type(v.getType()) + ": " + i + " -> " + v.getAttBdName() + "    --   Method: "
					+ v.getSetMethod().getName() + sequenceStr(v.getSequence()));
		}
		if (sonsList.size() > 0) {
			str.append(Idt.BR + "Sons:" + Idt.BR);
			for (int i = 0; i < sonsList.size(); i++) {
				HDSSon v = sonsList.get(i);
				str.append(Idt.BRT1 + i+ " - Field: "+ v.getField().getName() + " {" + Idt.BR + v.toString(Idt.T2) + Idt.T1 + "}");
			}
		}
		str.append(Idt.BR + "}");
		return str.toString();
	}

	private String sequenceStr(String str) {
		if (str == null) {
			return "";
		} else {
			return "    --    Sequence: " + str;
		}
	}

	private String type(Short type) {
		if (type == 0) {
			return "PrimaryKey";
		}
		if (type == 1) {
			return "ForeignKey";
		}
		if (type == 2) {
			return "AutoRelation";
		}
		if (type == 3) {
			return "NormalField";
		}
		return "error";
	}

	protected Class<T> getClazz() {
		return clazz;
	}

	protected void insert(T object) {
		System.out.println("Aqui " + object);
	}

}
