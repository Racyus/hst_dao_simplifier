package br.com.hst;

public class HSTDAOSPostgreSQL {
	
	public static final String EQUAL = "=";
	public static final String GREATER = ">";
	public static final String LESS = "<";
	public static final String NOTEQUAL = "<>";
	public static final String GREATEROREQUAL = ">=";
	public static final String LESSOREQUAL = "<=";
	public static final String BETWEEN = "BETWEEN";
	public static final String ILIKE = "ILIKE";
	public static final String LIKE = "LIKE";
	public static final String IN = "IN";

	protected static final String SELECT = Idt.BR + " SELECT " + Idt.BRT1;
	protected static final String WHERE = Idt.BR + " WHERE 1=1 ";

	protected static StringBuilder FROM(String table, String alias) {
		StringBuilder sql = new StringBuilder();
		sql.append(Idt.BR + " FROM " + table + " as " + alias);
		return sql;
	}
	
	protected static StringBuilder SEQUENCE(String sequence) {
		StringBuilder sql = new StringBuilder();
		sql.append(" (SELECT NEXTVAL('" + sequence+"')) ");
		return sql;
	}
	
	protected static StringBuilder INSERT(String table, StringBuilder fields) {
		StringBuilder sql = new StringBuilder();
		sql.append(Idt.BR + " INSERT INTO " + table + "  (" + fields+") VALUES ");
		return sql;
	}

	protected static StringBuilder JOIN(String collum, String table, String alias, String aliasFocus) {
		StringBuilder sql = new StringBuilder();
		sql.append(Idt.BRT1 + " JOIN " + table + " as " + alias);
		sql.append(Idt.BRT2 + " ON " + alias + Idt.DOT + collum + " = " + aliasFocus + Idt.DOT + collum);
		return sql;
	}

	protected static StringBuilder AND(String alias, String collum, String conditional) {
		StringBuilder sql = new StringBuilder();
		sql.append(Idt.BRT1 + " AND " + alias + Idt.DOT + collum + " " + conditional + " ? ");
		return sql;
	}

}
