package br.com.hst;

import java.util.HashMap;
import java.util.Map;

enum HDSSingleton {
	INSTANCE;

	@SuppressWarnings("rawtypes")
	private Map<String, HDSGenericModel> speakerMap = null;

	@SuppressWarnings("rawtypes")
	HDSSingleton() {
		speakerMap = new HashMap<String, HDSGenericModel>();
	}

	@SuppressWarnings("rawtypes")
	protected HDSGenericModel[] getGenericModel(Object... objects) {
		HDSGenericModel[] cores = new HDSGenericModel[objects.length];
		for (int i = 0; i < objects.length; i++) {
			addIfDontContains(objects[i]);
			cores[i] = speakerMap.get(objects[i].getClass().getSimpleName());
		}
		return cores;
	}

	private void addIfDontContains(Object modelTO) {
		if (!speakerMap.containsKey(modelTO.getClass().getSimpleName())) {
			speakerMap.put(modelTO.getClass().getSimpleName(), new HDSGenericModel<>(modelTO.getClass()));
		}
	}
	
	protected void printAllSpeakers() {
		speakerMap.forEach((k,v)->{
			System.out.println(v);
		});
	}

}
