package br.com.hst;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

class HDSInsert {

	@SuppressWarnings("rawtypes")
	private HDSGenericModel genModel = null;

	private StringBuilder values = null;

	private ArrayList<Object> whereValuesList = null;

	@SuppressWarnings("rawtypes")
	HDSInsert(ArrayList<Object> list, Object... objects) {
		whereValuesList = list;
		values = new StringBuilder();

		for (int i = 0; i < objects.length; i++) {
			if (genModel == null) {
				HDSGenericModel[] genericModel = HDSSingleton.INSTANCE.getGenericModel(objects[i]);
				genModel = genericModel[i];
			}
			if (objects[i].getClass().equals(genModel.getClazz())) {
				invokeGetters(objects[i]);
			}
			if (i != objects.length - 1)
				values.append(Idt.COMMA);

		}
		genModel.getFields();
	}

	private void invokeGetters(Object obj) {
		@SuppressWarnings("unchecked")
		ArrayList<HDSField> fields = genModel.getFieldsArrayList();

		values.append(Idt.BRT1 + " (");
		for (int i = 0; i < fields.size(); i++) {
			try {
				if (fields.get(i).getSequence() == null) {
					whereValuesList.add(fields.get(i).getGetMethod().invoke(obj));
					values.append(" ? ");
				} else {
					values.append(HSTDAOSPostgreSQL.SEQUENCE(fields.get(i).getSequence()));
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			if (i != fields.size() - 1)
				values.append(Idt.COMMA);
		}
		values.append(" ) ");
	}

	protected String getSQL() {
		StringBuilder sql = new StringBuilder();

		sql.append(HSTDAOSPostgreSQL.INSERT(genModel.getTable(), genModel.getFields()));
		sql.append(values);

		return sql.toString();
	}

}
