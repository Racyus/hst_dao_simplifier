package br.com.hst;

class HDSRegex {

	protected static final String PK = "Pk";
	protected static final String FK = "Fk";
	protected static final String AR = "Ar";

	protected static String dbName(String name) {
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < name.length(); i++) {
			if (isUpper(name.charAt(i) + "") && i > 0) {
				str.append("_");
			}
			str.append(name.charAt(i));
		}
		return str.toString().toLowerCase();
	}

	private static Boolean isUpper(String str) {
		if (str.equals(str.toUpperCase())) {
			return true;
		} else {
			return false;
		}
	}

}
