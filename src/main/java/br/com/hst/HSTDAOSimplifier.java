package br.com.hst;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.hst.exceptions.HDSException;

@SuppressWarnings("unchecked")
public class HSTDAOSimplifier<T> {

	private static final Short INSERT = 0;
//	private static final Short UPDATE = 1;
//	private static final Short DELETE = 2;
	private static final Short SELECT = 3;

	private Short action = null;

	private static final Logger logger = LogManager.getLogger(HSTDAOSimplifier.class);

	@SuppressWarnings("rawtypes")
	private HDSGenericModel[] speakers = null;

	private Map<String, HDSField> fieldsMap = new HashMap<>();

//	private StringBuilder query = null;
	private StringBuilder whereSQL = null;
	private ArrayList<Object> whereValuesList = null;

	private Object[] objects = null;

	public HSTDAOSimplifier(Object... objects) throws HDSException {
		this.objects = objects;
		speakers = HDSSingleton.INSTANCE.getGenericModel(objects);
		whereValuesList = new ArrayList<Object>();
		getAllFields();
	}

	public String getAllSelectSQL() throws HDSException {
		action = SELECT;
		StringBuilder sql = new StringBuilder();
		sql.append(HSTDAOSPostgreSQL.SELECT);
		sql.append(fieldsMap2StrBuff());
		sql.append(HSTDAOSPostgreSQL.FROM(speakers[0].getTable(), speakers[0].getTable() + 0));
		if (speakers.length > 1)
			sql.append(getJoins());
		return sql.toString();
	}

	public String getWhereSelectSQL() throws HDSException {
		StringBuilder sql = new StringBuilder();
		sql.append(getAllSelectSQL());
		if (whereSQL != null)
			sql.append(whereSQL);
		return sql.toString();
	}

	public void addWhereCondition(String collum, String conditional, Object value) throws HDSException {
		if (whereSQL == null) {
			whereSQL = new StringBuilder();
			whereSQL.append(HSTDAOSPostgreSQL.WHERE);
		}
		if (fieldsMap.containsKey(collum)) {
			whereValuesList.add(value);
			HDSField row = fieldsMap.get(collum);
			whereSQL.append(HSTDAOSPostgreSQL.AND(row.getAlias(), row.getAttBdName(), conditional));
		} else {
			throw new HDSException("Unidentified collum: " + collum);
		}
	}

	private void getAllFields() throws HDSException {
		for (int i = 0; i < speakers.length; i++) {
			ArrayList<HDSField> all = speakers[i].getFieldsArrayList();
			for (int j = 0; j < all.size(); j++) {
				HDSField row = all.get(j);
				if (!fieldsMap.containsKey(row.getField().getName())) {
					row.setAlias(speakers[i].getTable() + i);
					fieldsMap.put(row.getField().getName(), row);
				}
//				else {
//					throw new HDSException("Already contains: " + row);
//				}
			}
		}
	}

	public void printAllField() {
		fieldsMap.forEach((k, v) -> {
			System.out.println(k);
		});
	}

	public void setValues(PreparedStatement ps) {
		int count = 1;
		try {
			if (whereValuesList != null) {
				for (Object value : whereValuesList) {
					if (value instanceof String) {
						if (action == SELECT) {
							ps.setString(count++, "%" + ((String) value) + "%");
						} else {
							ps.setString(count++, (String) value);
						}
					} else if (value instanceof Integer) {
						ps.setInt(count++, (int) value);
					} else if (value instanceof Long) {
						ps.setLong(count++, (long) value);
					} else if (value instanceof Boolean) {
						ps.setBoolean(count++, (Boolean) value);
					} else if (value instanceof Byte) {
						ps.setByte(count++, (Byte) value);
					} else if (value instanceof Date) {
						ps.setDate(count++, (Date) value);
					} else if (value instanceof Double) {
						ps.setDouble(count++, (Double) value);
					}
				}
			}
		} catch (SQLException e) {
			logger.error("Error: " + e.getMessage() + "    - Cause: " + e.getCause());
		}
	}

	protected ArrayList<Object> getWhereValuesList() {
		return whereValuesList;
	}

	private StringBuilder getJoins() throws HDSException {
		StringBuilder sql = null;
		for (int i = 0; i < speakers.length; i++) {
			if (i != 0) {
				ArrayList<String> keysList = speakers[0].getKeysList();
				for (int j = 0; j < keysList.size(); j++) {
					if (speakers[i].getKeysList().contains(keysList.get(j))) {
						sql = HSTDAOSPostgreSQL.JOIN(keysList.get(j), speakers[i].getTable(), speakers[i].getTable() + i,
								speakers[0].getTable() + 0);
						break;
					}
				}
			}
		}
		if (sql == null) {
			throw new HDSException("Error: could not find a table relation. ");
		}
		return sql;
	}

	private StringBuilder fieldsMap2StrBuff() {
		StringBuilder sql = new StringBuilder();
		Object[] array = fieldsMap.keySet().toArray();
		for (int i = 0; i < array.length; i++) {
			HDSField row = fieldsMap.get(array[i]);
			sql.append(row.getAlias() + Idt.DOT + row.getAttBdName());
			if (i != array.length - 1)
				sql.append(Idt.BRT1C);
		}
		return sql;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < speakers.length; i++) {
			str.append(speakers[i]);
		}
		return str.toString();
	}

	public ArrayList<T> getResultSet(ResultSet rs) throws SQLException {
		ArrayList<T> list = new ArrayList<T>();
		if (speakers.length == 1) {
			while (rs.next()) {
				list.add((T) speakers[0].getResultSet(rs));
			}
		} else {
			ArrayList<Object> rawList = new ArrayList<Object>();
			while (rs.next()) {
				Object[] objects = new Object[speakers.length];
				for (int i = 0; i < speakers.length; i++) {
					objects[i] = speakers[i].getResultSet(rs);
				}
				rawList.add(objects);
			}
			list = assembler(rawList);
		}
		return list;
	}

	private ArrayList<T> assembler(ArrayList<Object> rawList) {
		ArrayList<T> list = new ArrayList<T>();
		Map<Object, Integer> listMap = new HashMap<Object, Integer>();
		Integer index = 0;
//		List<SpeakerCore> speakersList = Arrays.asList(speakers);
//		Map<Type, ArrayList<Type>> descendants = speakers[focus].getAllYourDescendants(speakers);

		for (int i = 0; i < rawList.size(); i++) {

			Object[] row = (Object[]) rawList.get(i);

			for (int j = 0; j < row.length; j++) {
				if (j == 0) {
					if (!list.contains(row[0])) {
						list.add((T) row[0]);
						listMap.put(row[0], index++);
//						System.out.println("Adicionou..");
					}
//					else {
//						System.out.println("Objeto igual.");
//					}
				} else {
					parentFinder(row[j], row, listMap, list);
				}
			}
			System.out.println();

		}

		return list;
	}

	private void parentFinder(Object obj, Object[] row, Map<Object, Integer> listMap, ArrayList<T> list) {
		for (int i = 0; i < row.length; i++) {
			HDSSon son = speakers[i].amIYourSon(obj);
			if (son != null) {
				Object objToInvokeOn = list.get(listMap.get(row[i]));
				if (son.getSuperType() != null) {// Proporção de 1 para muitos
					ArrayList<Object> oneToMany = oneToMany(son, objToInvokeOn);
					oneToMany.add(obj);
				}
//				else {//Proporção de 1 para 1
//					//TODO
//				}
//				System.out.println(objToInvokeOn + " ------ ");
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private ArrayList<Object> oneToMany(HDSSon son, Object objToInvokeOn) {
		ArrayList<Object> list = null;
		Method getMethod = son.getGetMethod();
		Method setMethod = son.getSetMethod();
		try {
			if ((ArrayList<Object>) getMethod.invoke(objToInvokeOn) == null) {
				setMethod.invoke(objToInvokeOn, new ArrayList());
			}
			list = (ArrayList<Object>) getMethod.invoke(objToInvokeOn);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void printAllSpeakers() {
		HDSSingleton.INSTANCE.printAllSpeakers();
	}

	// ---------- INSERT

	public String getInsertSQL() {
		action = INSERT;
		HDSInsert insert = new HDSInsert(whereValuesList, objects);
		return insert.getSQL();
	}

	// ---------- END

}
